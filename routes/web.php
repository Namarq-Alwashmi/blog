<?php

use Illuminate\Support\Facades\Route;
Route::get('/','PageController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('articles','ArticleController');
//Route::get('articles/{id}/edit','ArticleController');
Route::post('comments/{article}' , 'CommentController@store')->name('comments.store');
