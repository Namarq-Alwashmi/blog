@extends('layouts.app')
@section('title' , __('Home page'))

@section('content')

        <style>
            .jumbotron{
                background-image:url("{{ URL::asset('image/w6.jpg')}}") ;
                height: 600px;
                width: 1105px;
                opacity: 0.7;
            }
            div.transbox {
                margin:40px;
                background-color: #ffffff;
                border: 1px  ;
                opacity: 0.6;
            }
            div.transbox p {
                margin: 20%;
                font-weight: bold;
                font-family: 'hanimation';
                text-align: center;
                font-size:30px;
                color: #1f1c14;
            }

        </style>
       {{-- <h3>{{__('welcome to ')}} {{config('app.name')}}</h3>--}}
        <div class="jumbotron ">
            <div class="transbox">

                <p>حلم الأمس سيصبح حقيقية يوماً ما حارب لأجله.</p>
            </div>
        </div>
        <hr>
    <h3 class="text-warning">{{__('Latest articles ')}}</h3>
        <hr>
    <div class="row ">
        @forelse($articles as $article)

            @include('article._article_card')

        @empty
         {{__('No articles yet')}}
        @endforelse
    </div>
        <hr>
        <a class="btn btn-link-dark btn-outline-warning text-dark mb-2 pr-4" href="{{route('articles.index')}}"><b>{{__('Browse all article')}}</b></a>
        <hr>

@endsection
