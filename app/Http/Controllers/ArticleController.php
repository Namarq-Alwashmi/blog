<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class ArticleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $articles=Article::orderBy('id','DESC')->get();
        return view('article.index' ,compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $categories=Category::all()->pluck('title','id');
       // dd($categories);
        return view('article.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $request->validate([
            'title'=>'max:50|min:5|required',
            'content'=>'min:50|required',
            'categories'=>'required'

        ]);
       // dd($request->categories);
        //save data

       $user=Auth::user();
       //$categories=array_values($request->categories); //return array like this [1,2]
       // dd($categories);
         $data=$user->articles()->create($request->except('categories'));

      $article=Article::find($data->id);  //last article insert
      foreach ($request->categories as $category){
          $article->categories()->attach($category);
      }

     return redirect()->to('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //display article in the page
        return view('article.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */


    public function edit(Article $article)
    {
        if(Auth::id() != $article->user_id){
            return abort(401);
        }
        $categories=Category::pluck('title','id');
        //view categories the user is choice
        $articleCategories=$article->categories()->pluck('id')->toArray();
        return view('article.edit',compact('categories' ,'article' ,'articleCategories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        if(Auth::id() != $article->user_id){
            return abort(401);
        }

        $request->validate([
            'title'=>'max:50|min:20|required',
            'content'=>'min:140|required',
            'categories'=>'required'

        ]);
        //$article->title = $request['title'];
        //$article->content = $request['content'];
       // $article->save();
        //shortcut above
        $article->update($request->all());
       //يشيل التصنيف القديم اذ عدل ويحط الجديد
        $article->categories()->sync($request->categories);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        if(Auth::id() != $article->user_id){
            abort(401);
        }
     $article->delete();
     return redirect()->back();
    }
}
